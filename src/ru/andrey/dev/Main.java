package ru.andrey.dev;

import java.io.*;
import java.util.Scanner;

/**
 * Демонстрационный класс {@code Main}
 *
 * @author Рыжкин А.
 * @version 1.2
 */

public class Main {
    public static void main(String[] args) throws IOException {
        // Получаем данные с файла input.txt, проверяем, выполняем
        // вычисления и записываем результат в файл output.txt
        dataFile("input.txt", "output.txt");

        String str; // строка
        // Ввод строки через консоль.
        str = input();
        // Проверка на корректность ввода строки.
        if (!checkInput(str)) {
            System.exit(1);
        }

        // массив из 3 элементов, содержащий строки 2 операндов и знак операции.
        String[] data;
        // значение первого операнда.
        double firstValue;
        // значение второго операнда.
        double secondValue;
        // результат вычисления.
        double result;
        // разделяем строку str на массив строк data[] после каждого пробела.
        data = str.split(" ");

        // парсинг строки в double
        firstValue = Double.parseDouble(data[0]);
        secondValue = Double.parseDouble(data[1]);

        // метод, который выбирает операцию вычисления
        // по знаку и вызывает соответствующий метод класса Calculator.
        result = selectionOperation(firstValue, secondValue, data[2]);
        System.out.println(firstValue + " " + data[2] + " " + secondValue + " = " + result);
    }

    /**
     * Возвращает введённую строку через консоль, содержащая 2 операнда и знак операции.
     *
     * @return введённую строку.
     */
    public static String input() {
        Scanner reader = new Scanner(System.in);
        String str = reader.nextLine();
        reader.close();
        return str;
    }

    /**
     * Получает данные с файла, вызывает метод выбора операции
     * вычисления и записывает результат в выходной файл.
     *
     * @param pathInput  путь файла, по которому получает данные.
     * @param pathOutput путь файла, по которому записывает результат всех вычислений.
     * @throws IOException если неверный путь файлов или диск защищён от записи файлов.
     */
    public static void dataFile(String pathInput, String pathOutput) throws IOException {
        // массив из 3 элементов, содержащий строки 2 операндов и знак операции.
        String[] data;
        // исходная строка.
        String str;
        // значения первого и второго операнда, знак операции.
        double firstValue, secondValue, result;

        if (!checkFiles(pathInput, pathOutput)) {
            System.exit(1);
        }
        BufferedReader inputData = new BufferedReader(new FileReader(pathInput));
        BufferedWriter outData = new BufferedWriter(new FileWriter(pathOutput));


        // Получение данных из файла.
        while ((str = inputData.readLine()) != null) {
            if (!checkInput(str)) {
                continue;
            }
            data = str.split(" ");
            firstValue = Double.parseDouble(data[0]);
            secondValue = Double.parseDouble(data[1]);
            result = selectionOperation(firstValue, secondValue, data[2]);
            outData.write(firstValue + " " + data[2] + " " + secondValue + " = " + result + "\r\n");
        }
        inputData.close();
        outData.close();
    }

    /**
     * Возвращает {@code boolean} значение, проверяет входной и выходной файл на верно указанный путь к файлам
     * и чтение/запись файлов.
     *
     * @param pathInput  путь входного файла.
     * @param pathOutput путь выходного файла.
     * @return {@code true} или {@code false}
     * @throws IOException если неверный путь файлов или диск защищён от записи файлов.
     */
    public static boolean checkFiles(String pathInput, String pathOutput) throws IOException {
        BufferedReader inputData = null;
        BufferedWriter outData = null;
        try {
            inputData = new BufferedReader(new FileReader(pathInput));
        } catch (FileNotFoundException e) {
            System.out.println(e);
            return false;
        }
        try {
            outData = new BufferedWriter(new FileWriter(pathOutput));
        } catch (FileNotFoundException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    /**
     * Возвращает результат вычисления,т.е. выбирает операцию вычисления
     * по знаку и вызывает соответствующий метод класса {@code Calculator}.
     *
     * @param firstValue  значение первого операнда.
     * @param secondValue значение второго операнда.
     * @param sign        знак операции.
     * @return результат вычисления выбронной операции.
     */
    public static double selectionOperation(double firstValue, double secondValue, String sign) {
        int firstValueInt = (int) firstValue;
        int secondValueInt = (int) secondValue;
        double result = 0;

        switch (sign) {
            case "+":
                result = Calculator.add(firstValue, secondValue);
                break;
            case "-":
                result = Calculator.subtract(firstValue, secondValue);
                break;
            case "*":
                result = Calculator.multiplication(firstValue, secondValue);
                break;
            case "/":
                if (firstValue % 1 == 0 && secondValue % 1 == 0) {
                    result = Calculator.div(firstValueInt, secondValueInt);
                } else {
                    result = Calculator.div(firstValue, secondValue);
                }
                break;
            case "%":
                if (firstValue % 1 == 0 && secondValue % 1 == 0) {
                    result = Calculator.mod(firstValueInt, secondValueInt);
                }
                break;
            case "^":
                result = Calculator.pow(firstValue, secondValueInt);
                break;
            case "#":
                result = Calculator.sqrt(secondValue);
                break;
        }
        return result;
    }

    /**
     * Проверка корректного ввода на исходных данные:
     * "1 операнд" "2 операнд" "знак операции".
     *
     * @param str строка
     * @return возвращает true, если ошибок в строке не обнаружено.
     * @throws NullPointerException  если пустая строка.
     * @throws NumberFormatException если неверные значения операндов.
     */
    public static boolean checkInput(String str) {
        // разбиение строки после пробела на массив строк.
        String[] inputData = str.split(" ");
        // проверка ввода полной строки
        if (str.length() == 0) {
            throw new NullPointerException("Пустая строка!");
        }
        // если не введён операнд или знак операции
        if (inputData.length != 3) {
            System.out.println("Не введён операнд или знак операции!");
            return false;
        }
        // проверка на соответствие типа Double
        try {
            Double.parseDouble(inputData[0]);
        } catch (NumberFormatException e) {
            System.out.println("Неверное значение 1 операнда! " + e);
            return false;
        }
        try {
            Double.parseDouble(inputData[1]);
        } catch (NumberFormatException e) {
            System.out.println("Неверное значение 2 операнда! " + e);
            return false;
        }
        // проверка на ввод знака операции, состоящего только из 1 одного символа
        if (inputData[2].length() != 1) {
            System.out.println("Знак операции состоит только из 1 символа!");
        }
        char[] sign = {'+', '-', '*', '/', '%', '^', '#'};
        boolean flag = true;
        // проверка на ввод знака операции
        for (int i = 0; i < sign.length; i++) {
            if (sign[i] == (inputData[2]).charAt(0)) {
                flag = false;
                break;
            }
        }
        if (flag) {
            System.out.println("Неверный знак операции!");
            return false;
        }
        if ((Double.parseDouble(inputData[0]) % 1 != 0 || Double.parseDouble(inputData[1]) % 1 != 0) && inputData[2].charAt(0) == '%') {
            System.out.println("Неверные значения операндов при выделении остатка!");
            return false;
        }
        return true;
    }
}